using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class LoadThanksLevel : MonoBehaviour
{
    public string LoadLevel;

    public void OpenLevel()
    {
        SceneManager.LoadScene(LoadLevel);
    }
}
