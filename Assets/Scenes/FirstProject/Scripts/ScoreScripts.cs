using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScripts : MonoBehaviour
{
    public Transform playerTransform;
    public Text scoreText;

    private void Update()
    {
        scoreText.text = (playerTransform.position.z+6).ToString("0");
    }
}
