using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
//using UnityEngine.;


public class GameMode : MonoBehaviour
{

    public GameObject LevelWonUI;
    public PlayerScript PlayerScript;
    public float restartDelay = 2f;

    protected bool gameHasEnded = false;

    public void enablingLevelWon()
    {
       LevelWonUI.SetActive(true);
    }

    public void GameWon()
    {
        Debug.Log("Won!");
        //FindObjectOfType<Level>
        enablingLevelWon();
    }

    public void EndGame()
    {
        gameHasEnded = true;
        Debug.Log("Game Ended");

        Invoke("Restart", restartDelay);

        //PlayerScript.transform.position = PlayerScript.initialPlayerLocation;
        //PlayerScript.enabled = true;
    }

    private void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
