using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{

    public Rigidbody rb;
    public float forwardForce = 2000f;
    private float sideForce = 500f;
    public Vector3 initialPlayerLocation;

    public float dx; //directional val for android tilt sensing;
    public bool androidCheck;
    //protected float dragForce;
    //public Transform playerTransform;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        sideForce = forwardForce / 15;

        initialPlayerLocation = transform.position;

        androidCheck = false;
        if(Application.platform == RuntimePlatform.Android)
        {
            androidCheck= true;
        }

        //Input.gyro.enabled = true;
        dx = 0.0f; //for android tilt sensing;

        //dragForce = sideForce / 4;
        //rb.AddForce(0, 200, 500);
        //Debug.Log("Hello World");
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.y < -2)
        {
            FindObjectOfType<GameMode>().EndGame();
        }

        if (androidCheck)
        {
            dx = Input.acceleration.x;
        }
    }

    //using FixedUpdate insted of Update as we are using physics and while using physics it's a good practice to use FixedUpdate 
    private void FixedUpdate()
    {
        rb.AddForce (0,0,forwardForce* Time.deltaTime);
        //sideForce += (forwardForce / 8) * Time.deltaTime;
        
        if (Input.GetKey("d"))
        {
            rb.AddForce(sideForce* Time.deltaTime,0,0,ForceMode.VelocityChange);
        }
        else if(Input.GetKey("a"))
        {
            rb.AddForce(-1*sideForce * Time.deltaTime, 0, 0,ForceMode.VelocityChange);
        }
        //else if (transform.position.x > 0)
        //{
        //    rb.AddForce(dragForce * Time.deltaTime, 0, 0);
        //}

        //using android tilt  as control;
        //dx = Input.acceleration.x * Time.deltaTime*sideForce;

        //Debug.Log(Input.gyro.ToString());

        if(androidCheck)
        {
            rb.AddForce(dx*sideForce*Time.deltaTime,0,0,ForceMode.VelocityChange);
        }
    }
}
