using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    public PlayerScript playerScript;
    public GameMode GameManager;
    
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "Enemy")
        {
            Debug.Log(playerScript.transform.position);
            playerScript.enabled = false;
            
            FindObjectOfType<GameMode>().EndGame();
        }

        //will not work here as this functiion is called when an object is collind with something;
        //if(playerScript.transform.position.y< -10)
        //{
        //    FindAnyObjectByType<GameObject>().EndGame();
        //}

    }
}
