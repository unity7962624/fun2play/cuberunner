using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinGame : MonoBehaviour
{
    public GameMode GameObject;
    public void OnTriggerEnter(Collider other)
    {
        GameObject.GameWon();
    }
}
